"""
PyRandomNewWorld : generate new worlds for Europa Universalis 4
Copyright (C) 2024 Askywhale

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import logging
import sys
import datetime
import random
import math
import copy
import os
import shutil
import psutil
import multiprocessing
import threading
import signal
import numpy as np # for memory not speed
from PIL import Image, ImageOps
from pathlib import Path

def try_to_generate(nmap):
    SQUARE_DIAMOND_COARSENESS = 50 # 0..100
    SEA_HEIGHT = 95 # Empirical from standard data : 50..sea...|95..land..255 ; 42% of land in [95,100]
    WASTELAND_HEIGHT = 130 # lets say wasteland = high mountains
    MIN_BORDER_IIR = 32
    CUMUL_HEIGHT_STEPS_BASE = [0,50,95,110,130,255]        
    CUMUL_HEIGHT_STEPS = [0,50,80,140,160,255] # some moutains    
    MIN_HEIGHT = 50
    MIN_LAND_PRC = 20
    MAX_LAND_PRC = 45
    MIN_AREA_SIZE = 50
    MIN_TERRITORY_SIZE = MIN_AREA_SIZE
    AVG_TERRITORY_SIZE = 400    
    MAX_TERRITORY_SIZE = 1200
    MIN_SEA_SIZE = 4000
    AVG_SEA_SIZE = 12000
    MAX_SEA_SIZE = 36000
    NORTH_EDGE_SIZE_DISTANCE = 200 # *X .. *1 next the north or south edge
    NORTH_EDGE_SIZE_MLT = 4.5
    SOUTH_EDGE_SIZE_DISTANCE = 90
    SOUTH_EDGE_SIZE_MLT = 3
    NORTH_TO_EQUATOR_DISTANCE = 1200 # approx
    TRADE_CENTER_MIN_DIST = 90    
    REGION_CENTER_MIN_DIST = 350 # standard Rnw : 5 regions on 1408*1920, 18 on 2048*2048
    MIN_SIZE_SET_REGION = 30 # 4*4 (512*512) : no region ; 4*8 (512*1024) : about 2 regions
    MAX_RIVERS_BY_1_1 = 80 # seems high, but in fact 99% will fail    
    MIN_RIVER_START = 105
    MAX_STRAIT_LEN = 20
    MAX_TOTAL_TERRITORIES = 1024 # RNW just forbid lands+wastelands+seas+lakes > 1024
    MIN_LAND_TERRITORIES_16_16 = 850 # this is the real goal of this script ; but each miss costs a few hours
    MAX_SEAS_EXCLUDING_LAKES = 200 # RNW also forbid more (names)
    MAX_LAKES = 100 # RNW also forbid more (names)
    LIM_RISK_1024 = 120 # sizex*sizey ; if size > (this constant), a size corresponding multiplier 
    # is applied (so, less territories)
    MIN_SIZE = 2 # 2, 8 or 16
    MAX_SIZE = 16 # 2, 8 or 16
    MAX_DIST = 30000 # in distmaps    
    
    nmap+=1 # starts at 0 => starts at 1
    starttime=datetime.datetime.now()
    starttime_str=starttime.strftime("%y%m%d%H%M%S")
    name="pyrnw_"+starttime_str+"_{0:03d}".format(nmap)

    # full space : size=18x16
    size_ok = False
    while not size_ok:
        sizes = [n for n in [2,4,8,16,16] if n>=MIN_SIZE and n<=MAX_SIZE] # 16,16 because half of 16xX fails
        sizex = random.choice(sizes)
        sizey = random.choice(sizes+[sizex,sizex]) # prefer same size
        size_ok = (sizex,sizey)!=(2,16) and (sizex,sizey)!=(16,2) # too strange

    bmpsizex = sizex*128
    bmpsizey = sizey*128
    bmpsteps = int(0.5+math.log2(max(bmpsizex, bmpsizey))) # 0.5 should not be needed
    
    # Averages, CPython 3.8
    # bmpsteps - tiles : min time 2010 setup, min time 2020 setup, RAM, useful territories 
    # 8 - 2x2 : 3min, 2min, 70Mb, 35
    # 9 - 4x4 : 80min, 30min, 200Mb, 140
    # 10 - 8x8 : 10h, 1h, 2.5Gb, 500
    # 11 - 16x16 : /, 5h, 21Gb, 800 -- no point with the 1024 limit
    logging.info("Will now generate map "+str(nmap)+", a "+str(sizex)+"*"+str(sizey)+" tile (maybe "+
        str(int((sizex*sizey)**1.5)//8)+" minutes and "+str(int(((sizex*sizey)**2)/3.5+50))+"Mb RAM)")
    # P edges : size=2 : p=1/15 ; size=4 : p=1/13 ; size=16 : p=1   

    if sizex*sizey>LIM_RISK_1024:
        north_edge = south_edge = True
        # Too much territories, lets do some big lands with wastelands instead, not too much mountains
        CUMUL_HEIGHT_STEPS = [0,50,80,104,113,255] 
        mlt = sizex*sizey/LIM_RISK_1024
        MIN_AREA_SIZE = int(MIN_AREA_SIZE*mlt)
        MIN_TERRITORY_SIZE = MIN_AREA_SIZE
        AVG_TERRITORY_SIZE = int(AVG_TERRITORY_SIZE*mlt)
        MAX_TERRITORY_SIZE = int(MAX_TERRITORY_SIZE*mlt)    
    if sizey==16:
        north_edge = south_edge = True
    else:
        north_edge = random.randint(0,16-sizey)==0    
        south_edge = (not north_edge) and (random.randint(0,15-sizey)==0)            

    def gethm(x, y):
        if x>=0 and y>=0 and x<bmpsizex+1 and y<bmpsizey+1:
            return heightmap[x][y]
        else:
            return 0

    def create_distmap(x=None, y=None, mode="bysea"): 
        """Not in pixels!        
        mode:bysea (x5), byland (x20), toblurredland (x5), byseatoborder (x1), bynothingtoborder (x1), tosamesea (x1)
        """
        BASE_LAND_DIST = 10        
        distmap = [[MAX_DIST for y1 in range(bmpsizey+1)] for x1 in range(bmpsizex+1)]        
        if x!=None and y!=None:
            distmap[x][y] = 0        
            WINDOWS = {"bysea":MAX_SEA_SIZE**.5, "byland":MAX_TERRITORY_SIZE**.5, "tosamesea":MAX_SEA_SIZE**.5}
            usefull_half_window = 3*int(WINDOWS[mode])
            min_x = max(0, x - usefull_half_window)
            min_y = max(0, y - usefull_half_window)
            max_x = min(bmpsizex, x + usefull_half_window)
            max_y = min(bmpsizey, y + usefull_half_window)
        if mode=="bysea":
            change = True            
            while change:
                change = False
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(max_y,min_y-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x+1][y]+5
                            change = True
                for x in range(max_x,min_x-1,-1):
                    for y in range(max_y-1,min_y-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y+1]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x][y+1]+5
                            change = True
                for x in range(min_x+1,max_x+1):
                    for y in range(min_y,max_y+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x-1][y]+5
                            change = True
                for x in range(min_x, max_x+1):
                    for y in range(min_y+1, max_y+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y-1]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x][y-1]+5
                            change = True        
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(max_y-1,min_y-1,-1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y+1]+7<distmap[x][y] and
                                heightmap[x+1][y]<SEA_HEIGHT and heightmap[x][y+1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x+1][y+1]+7
                            change = True
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(min_y+1, max_y+1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y-1]+7<distmap[x][y] and
                                heightmap[x+1][y]<SEA_HEIGHT and heightmap[x][y-1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x+1][y-1]+7
                            change = True
                for x in range(min_x+1, max_x+1):
                    for y in range(max_y-1,min_y-1,-1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y+1]+7<distmap[x][y] and
                                heightmap[x-1][y]<SEA_HEIGHT and heightmap[x][y+1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x-1][y+1]+7
                            change = True
                for x in range(min_x+1, max_x+1):
                    for y in range(min_y+1, max_y+1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y-1]+7<distmap[x][y] and
                                heightmap[x-1][y]<SEA_HEIGHT and heightmap[x][y-1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x-1][y-1]+7
                            change = True
        elif mode=="byland":
            rangelist = []
            rangelistd = []
            for i in range(256):
                if i<SEA_HEIGHT:
                    rangelist += [1000]
                    rangelistd += [1414]
                else:
                    rangelist += [BASE_LAND_DIST+i-SEA_HEIGHT]                    
                    rangelistd += [int(1.414*(BASE_LAND_DIST+i-SEA_HEIGHT))]                    
            change = True                    
            while change:
                change = False
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(max_y,min_y-1,-1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x+1][y]+rangelist[heightmap[x+1][y]]<distmap[x][y]):
                            distmap[x][y] = distmap[x+1][y]+rangelist[heightmap[x+1][y]]
                            change = True
                for x in range(max_x,min_x-1,-1):
                    for y in range(max_y-1,min_y-1,-1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x][y+1]+rangelist[heightmap[x][y+1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x][y+1]+rangelist[heightmap[x][y+1]]
                            change = True
                for x in range(min_x+1,max_x+1):
                    for y in range(min_y,max_y+1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x-1][y]+rangelist[heightmap[x-1][y]]<distmap[x][y]):
                            distmap[x][y] = distmap[x-1][y]+rangelist[heightmap[x-1][y]]
                            change = True
                for x in range(min_x, max_x+1):
                    for y in range(min_y+1, max_y+1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x][y-1]+rangelist[heightmap[x][y-1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x][y-1]+rangelist[heightmap[x][y-1]]
                            change = True
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(max_y-1,min_y-1,-1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x+1][y+1]+rangelistd[heightmap[x+1][y+1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x+1][y+1]+rangelistd[heightmap[x+1][y+1]]
                            change = True
                for x in range(min_x+1,max_x+1):
                    for y in range(max_y-1,min_y-1,-1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x-1][y+1]+rangelistd[heightmap[x-1][y+1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x-1][y+1]+rangelistd[heightmap[x-1][y+1]]
                            change = True
                for x in range(min_x+1,max_x+1):
                    for y in range(min_y+1,max_y+1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x-1][y-1]+rangelistd[heightmap[x-1][y-1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x-1][y-1]+rangelistd[heightmap[x-1][y-1]]
                            change = True
                for x in range(max_x-1,min_x-1,-1):
                    for y in range(min_y+1, max_y+1):
                        if (heightmap[x][y]>=SEA_HEIGHT and
                                distmap[x+1][y-1]+rangelistd[heightmap[x+1][y-1]]<distmap[x][y]):
                            distmap[x][y] = distmap[x+1][y-1]+rangelistd[heightmap[x+1][y-1]]
                            change = True
        elif mode=="toblurredland":   # not really blurry
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    if heightmap[x][y]>=SEA_HEIGHT:
                        distmap[x][y] = 0
            change = True
            while change:
                change = False
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x+1][y]+5
                            change = True
                for x in range(bmpsizex,-1,-1):
                    for y in range(bmpsizey-1,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y+1]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x][y+1]+5
                            change = True
                for x in range(1,bmpsizex+1):
                    for y in range(0,bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x-1][y]+5
                            change = True
                for x in range(0, bmpsizex+1):
                    for y in range(1, bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y-1]+5<distmap[x][y]:
                            distmap[x][y] = distmap[x][y-1]+5
                            change = True        
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey-1,-1,-1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y+1]+7<distmap[x][y] and
                                heightmap[x+1][y]<SEA_HEIGHT and heightmap[x][y+1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x+1][y+1]+7
                            change = True
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(1, bmpsizey+1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y-1]+7<distmap[x][y] and
                                heightmap[x+1][y]<SEA_HEIGHT and heightmap[x][y-1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x+1][y-1]+7
                            change = True
                for x in range(1, bmpsizex+1):
                    for y in range(bmpsizey-1,-1,-1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y+1]+7<distmap[x][y] and
                                heightmap[x-1][y]<SEA_HEIGHT and heightmap[x][y+1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x-1][y+1]+7
                            change = True
                for x in range(1, bmpsizex+1):
                    for y in range(1, bmpsizey+1):
                        if (heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y-1]+7<distmap[x][y] and
                                heightmap[x-1][y]<SEA_HEIGHT and heightmap[x][y-1]<SEA_HEIGHT):
                            distmap[x][y] = distmap[x-1][y-1]+7
                            change = True            
            # more sea next to borders
            if north_edge:
                if south_edge:
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            dist = min(x, (bmpsizex+1-x))
                            ratio = MIN_BORDER_IIR*0.5/(dist+1)
                            if ratio>1:
                                distmap[x][y] = int(distmap[x][y]*ratio)
                else:
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            dist = min(x, (bmpsizex+1-x), (bmpsizey+1-y))
                            ratio = MIN_BORDER_IIR*0.5/(dist+1)
                            if ratio>1:
                                distmap[x][y] = int(distmap[x][y]*ratio)
            elif south_edge:            
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        dist = min(x, y, (bmpsizex+1-x))
                        ratio = MIN_BORDER_IIR*0.5/(dist+1)
                        if ratio>1:
                            distmap[x][y] = int(distmap[x][y]*ratio)
            else:
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        dist = min(x, y, (bmpsizex+1-x), (bmpsizey+1-y))
                        ratio = MIN_BORDER_IIR*0.5/(dist+1)
                        if ratio>1:
                            distmap[x][y] = int(distmap[x][y]*ratio)            
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    distmap[x][y] //= 5
        elif mode=="byseatoborder":
            if north_edge:
                if south_edge:
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            if x==0 or x==bmpsizex:
                                distmap[x][y] = 0
                else:
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            if x==0 or x==bmpsizex or y==bmpsizey:
                                distmap[x][y] = 0
            elif south_edge:
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if x==0 or y==0 or x==bmpsizex:
                            distmap[x][y] = 0
            else:
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if x==0 or y==0 or x==bmpsizex or y==bmpsizey:
                            distmap[x][y] = 0
            change = True
            while change:
                change = False
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y]+1<distmap[x][y]:
                            distmap[x][y] = distmap[x+1][y]+1
                            change = True
                for x in range(bmpsizex,-1,-1):
                    for y in range(bmpsizey-1,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y+1]+1<distmap[x][y]:
                            distmap[x][y] = distmap[x][y+1]+1
                            change = True
                for x in range(1,bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y]+1<distmap[x][y]:
                            distmap[x][y] = distmap[x-1][y]+1
                            change = True
                for x in range(bmpsizex+1):
                    for y in range(1,bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y-1]+1<distmap[x][y]:
                            distmap[x][y] = distmap[x][y-1]+1
                            change = True                   
        elif mode=="bynothingtoborder":
            if north_edge:
                if south_edge:                            
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            if x==0 or x==bmpsizex:
                                distmap[x][y] = 0            
                else:
                    for x in range(bmpsizex+1):
                        for y in range(bmpsizey+1):
                            if x==0 or x==bmpsizex or y==bmpsizey:
                                distmap[x][y] = 0
            elif south_edge:                            
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if x==0 or y==0 or x==bmpsizex:
                            distmap[x][y] = 0            
            else:
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if x==0 or y==0 or x==bmpsizex or y==bmpsizey:
                            distmap[x][y] = 0
            change = True
            while change:
                change = False
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y]+1<distmap[x][y] and territorymap[x][y]==no_territory:
                            distmap[x][y] = distmap[x+1][y]+1
                            change = True
                for x in range(bmpsizex,-1,-1):
                    for y in range(bmpsizey-1,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y+1]+1<distmap[x][y] and territorymap[x][y]==no_territory:
                            distmap[x][y] = distmap[x][y+1]+1
                            change = True
                for x in range(1,bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y]+1<distmap[x][y] and territorymap[x][y]==no_territory:
                            distmap[x][y] = distmap[x-1][y]+1
                            change = True
                for x in range(bmpsizex+1):
                    for y in range(1,bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y-1]+1<distmap[x][y] and territorymap[x][y]==no_territory:
                            distmap[x][y] = distmap[x][y-1]+1
                            change = True                            
        elif mode=="tosamesea":
            sea = territorymap[x][y]
            if sea not in seas_centers: # TODO check if this is really useful
                logging.error("sea not in seas_centers : "+str(x)+","+str(y))
            change = True
            nbc = 0
            while change:
                change = False
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x+1][y]+1<distmap[x][y] and territorymap[x][y]==sea:
                            distmap[x][y] = distmap[x+1][y]+1
                            change = True
                            nbc += 1
                for x in range(bmpsizex,-1,-1):
                    for y in range(bmpsizey-1,-1,-1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y+1]+1<distmap[x][y] and territorymap[x][y]==sea:
                            distmap[x][y] = distmap[x][y+1]+1
                            change = True
                            nbc += 1
                for x in range(1,bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x-1][y]+1<distmap[x][y] and territorymap[x][y]==sea:
                            distmap[x][y] = distmap[x-1][y]+1
                            change = True
                            nbc += 1
                for x in range(bmpsizex+1):
                    for y in range(1,bmpsizey+1):
                        if heightmap[x][y]<SEA_HEIGHT and distmap[x][y-1]+1<distmap[x][y] and territorymap[x][y]==sea:
                            distmap[x][y] = distmap[x][y-1]+1
                            change = True   
                            nbc += 1
            # logging.debug("distmap tosamesea "+str(sea["color"][0])+","+
            #    str(sea["color"][1])+","+str(sea["color"][2])+" - "+str(sea["x"])+","+str(sea["y"])+" : "+str(nbc))
        else: 
            raise Exception("What mode ?")
        return np.array(distmap, dtype=np.int16)
        
    def get_color(sea = False):
        while True:
            ok = True
            if sea:
                color = (random.randint(10,50),random.randint(10,50),random.randint(100,255))    
                for c in seas_centers:
                    if c["color"] == color:
                        ok = False
                        break            
            else:
                color = (random.randint(80,255),random.randint(80,255),random.randint(10,99))
                for c in territories_centers:
                    if c["color"] == color:
                        ok = False
                        break
            if ok:
                return color

    def get_size_edge_mlt(y):
        size_edge_mlt = 1
        if north_edge:
            size_edge_mlt = max(size_edge_mlt, 
                1+(NORTH_EDGE_SIZE_MLT-1)*(NORTH_EDGE_SIZE_DISTANCE-y)/NORTH_EDGE_SIZE_DISTANCE)
        if south_edge:
            size_edge_mlt = max(size_edge_mlt, 
                1+(SOUTH_EDGE_SIZE_MLT-1)*(SOUTH_EDGE_SIZE_DISTANCE-bmpsizey+y)/SOUTH_EDGE_SIZE_DISTANCE)
        return size_edge_mlt

    # Land : heightmap ; one line and one column are lost
    heightmap = [[50 for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
    prc_land = 0
    while prc_land < MIN_LAND_PRC or prc_land > MAX_LAND_PRC:
        # square diamond algorithm
        rand_mlt = 128
        nbdone = 0
        for i in range(bmpsteps):
            # diamond
            stepsize = 2**(bmpsteps-i)            
            if stepsize<=min(bmpsizex, bmpsizey):
                for x in range(0, bmpsizex, stepsize):
                    for y in range(0, bmpsizey, stepsize):
                        heightmap[x+stepsize//2][y+stepsize//2] = max(0, min(255, 
                            random.randint(-rand_mlt,rand_mlt) + int((0.5 +
                            heightmap[x][y] + heightmap[x+stepsize][y] +
                            heightmap[x][y+stepsize] + heightmap[x+stepsize][y+stepsize]) / 4)))
                        nbdone+=1
            # square
            even = False
            for x in range(0, bmpsizex+1, stepsize//2):
                for y in range(0, bmpsizey+1, stepsize//2):
                    if even:
                        heightmap[x][y] = max(0, min(255, random.randint(-rand_mlt,rand_mlt) + int(0.5 +
                            (gethm(x-stepsize//2, y)+gethm(x+stepsize//2, y)+
                            gethm(x, y-stepsize//2)+gethm(x, y+stepsize//2)) / 4)))
                        nbdone+=1
                    even = not even
            
            if stepsize<=min(bmpsizex, bmpsizey):
                rand_mlt = int(rand_mlt * (2**(-(1-SQUARE_DIAMOND_COARSENESS/100))))

            # sea on borders of the whole map
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    if north_edge:
                        if south_edge:
                            dist = min(x, (bmpsizex+1-x))
                        else:
                            dist = min(x, (bmpsizex+1-x), (bmpsizey+1-y))
                    elif south_edge:
                        dist = min(x, y, (bmpsizex+1-x))
                    else:
                        dist = min(x, y, (bmpsizex+1-x), (bmpsizey+1-y))
                    if dist<MIN_BORDER_IIR:
                        heightmap[x][y] = int(heightmap[x][y]*(dist/MIN_BORDER_IIR))


        # too low or too high
        height_updates = []
        for v in range(256):
            vu = 0
            for i in range(0,len(CUMUL_HEIGHT_STEPS)):
                if v>CUMUL_HEIGHT_STEPS[i] and v<=CUMUL_HEIGHT_STEPS[i+1]:
                    vu = int(0.5+(CUMUL_HEIGHT_STEPS_BASE[i] + 
                        (CUMUL_HEIGHT_STEPS_BASE[i+1]-CUMUL_HEIGHT_STEPS_BASE[i])*
                        (v-CUMUL_HEIGHT_STEPS[i])/(CUMUL_HEIGHT_STEPS[i+1]-CUMUL_HEIGHT_STEPS[i])))
            height_updates += [vu]
        for x in range(bmpsizex+1):
            for y in range(bmpsizey+1):
                heightmap[x][y] = height_updates[heightmap[x][y]]

        #remove small areas
        mainchange = True
        while mainchange:
            codemap = [[y*(bmpsizex+1)+x for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
            change = True
            while change:
                change = False
                nc = 0
                for x in range(bmpsizex-1,-1,-1):
                    for y in range(bmpsizey+1):
                        if ((heightmap[x][y]<SEA_HEIGHT) == (heightmap[x+1][y]<SEA_HEIGHT)) and (
                                codemap[x][y]!=codemap[x+1][y]):
                            codemap[x][y] = min(codemap[x+1][y], codemap[x][y])
                            codemap[x+1][y] = codemap[x][y]
                            change = True
                for x in range(bmpsizex+1):
                    for y in range(bmpsizey-1,-1,-1):                
                        if ((heightmap[x][y]<SEA_HEIGHT) == (heightmap[x][y+1]<SEA_HEIGHT)) and (
                                codemap[x][y]!=codemap[x][y+1]):
                            codemap[x][y] = min(codemap[x][y+1], codemap[x][y])
                            codemap[x][y+1] = codemap[x][y]
                            change = True                                
                for x in range(1,bmpsizex+1):
                    for y in range(bmpsizey+1):
                        if ((heightmap[x][y]<SEA_HEIGHT) == (heightmap[x-1][y]<SEA_HEIGHT)) and (
                                codemap[x][y]!=codemap[x-1][y]):
                            codemap[x][y] = min(codemap[x-1][y], codemap[x][y])
                            codemap[x-1][y] = codemap[x][y]
                            change = True
                for x in range(bmpsizex+1):
                    for y in range(1,bmpsizey+1):                                
                        if ((heightmap[x][y]<SEA_HEIGHT) == (heightmap[x][y-1]<SEA_HEIGHT)) and (
                                codemap[x][y]!=codemap[x][y-1]):
                            codemap[x][y] = min(codemap[x][y-1], codemap[x][y])
                            codemap[x][y-1] = codemap[x][y]
                            change = True
            sizes = [0 for n in range((bmpsizex+1)*(bmpsizey+1))]
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    sizes[codemap[x][y]] += 1
            tochange = []
            for n in range((bmpsizex+1)*(bmpsizey+1)):
                if sizes[n]>0 and sizes[n]<MIN_AREA_SIZE*(get_size_edge_mlt(n//(bmpsizex+1))**2):
                    tochange += [n]
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    if codemap[x][y] in tochange:
                        if heightmap[x][y]<SEA_HEIGHT:
                            heightmap[x][y] = SEA_HEIGHT                            
                        else:
                            heightmap[x][y] = SEA_HEIGHT-1             
            mainchange = len(tochange)>0

        nb_land = 0
        for x in range(bmpsizex+1):
            for y in range(bmpsizey+1):
                if heightmap[x][y]>=SEA_HEIGHT:
                    nb_land += 1
        prc_land = nb_land*100 / ((bmpsizex+1)*(bmpsizey+1))
        logging.debug("land : "+str(prc_land)+"%")
        nb_water = (bmpsizex+1)*(bmpsizey+1) - nb_land
    
    map_to_land = create_distmap(None, None, "toblurredland")    

    hbmp = Image.new("L", (bmpsizex, bmpsizey), "black")
    pixels = hbmp.load()
    for x in range(bmpsizex):
        for y in range(bmpsizey):
            pixels[x, y] = heightmap[x][y]
    hbmp.save("tiles/data/"+name+"_h.bmp")

    # rivers
    rivermap = [[False for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
    confluencemap = [[False for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
    riverstartmap = [[False for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
    nb_river_tries = int(MAX_RIVERS_BY_1_1 * sizex * sizey * prc_land / 100) 
    nb_river = 0
    for n in range(nb_river_tries):
        x = random.randint(1,bmpsizex-1)
        y = random.randint(1,bmpsizey-1)
        if (heightmap[x][y] > MIN_RIVER_START and not rivermap[x][y] and 
                ((not north_edge) or y>20) and ((not south_edge) or y<bmpsizey-20)):
            failed = False
            backup = copy.deepcopy(rivermap)
            befores = []
            confluenced = False
            while heightmap[x][y]>=SEA_HEIGHT-5:
                if rivermap[x][y]:
                    confluenced = True
                    # logging.debug("Adding a confluence")
                    break
                else:
                    rivermap[x][y] = True
                    befores += [(x, y)]
                best = None
                best_height = 1e6
                for delta in ((-1,0),(0,-1),(1,0),(0,1)):
                    xx = x + delta[0]
                    yy = y + delta[1]
                    if xx<0 or xx>bmpsizex or yy<0 or yy>bmpsizey:
                        break
                    # confluence : go for it
                    if rivermap[xx][yy] and (xx,yy) not in befores:
                        best = (xx,yy)
                        best_height = heightmap[xx][yy]                
                        break
                    # no squares in the river map
                    no_square = True
                    for delta2 in ((-1,0),(0,-1),(1,0),(0,1)):
                        if (xx+delta2[0],yy+delta2[1]) in befores[:-1]:
                            no_square = False                    
                    for delta2 in ((-1,-1),(1,-1),(1,1),(-1,1)):
                        if (xx+delta2[0],yy+delta2[1]) in befores[:-2]:
                            no_square = False
                    if not no_square:
                        continue
                    # best next river pixel : lowest one
                    if heightmap[xx][yy]<best_height and (xx,yy) not in befores:
                        best = (xx,yy)
                        best_height = heightmap[xx][yy]
                if best==None:
                    failed = True
                    break
                x, y = best            
            if len(befores)<20*(1+100*nb_river/nb_river_tries):
                failed = True
            if failed:
                rivermap = backup
            else:
                if confluenced:                    
                    confluencemap[befores[-1][0]][befores[-1][1]] = True
                else:
                    riverstartmap[befores[0][0]][befores[0][1]] = True
                nb_river += 1
                logging.debug("Added a river of len : "+str(len(befores)))
            
    # BMP : r
    # Palette : 0=main river start, 1=end of sub-river, 3-11=river, 254=sea, 255=land
    hbmp = Image.new("P", (bmpsizex, bmpsizey), "black")
    pixels = hbmp.load()
    palette = [[0,0,0] for i in range(256)]
    palette[0] = [0, 255, 0]
    palette[1] = [255, 0, 0]
    for n in range(9):
        palette[n+3] = [0, 225*(8-n)//8, 255]
    palette[254] = [122, 122, 122]
    palette[255] = [255, 255, 255]
    hbmp.putpalette(sum(palette, [])) #flatten
    for x in range(bmpsizex):
        for y in range(bmpsizey):
            if confluencemap[x][y]:
                pixels[x, y] = 1
            elif riverstartmap[x][y]:
                pixels[x, y] = 0
            elif rivermap[x][y]:
                pixels[x, y] = 3
            elif heightmap[x][y]<SEA_HEIGHT:
                pixels[x, y] = 254
            else:
                pixels[x, y] = 255                
    hbmp.save("tiles/data/"+name+"_r.bmp")

    byseatobordermap = create_distmap(None, None, "byseatoborder")

    # Land territories
    nb_tries = 0
    ok_territories_and_wasteland = False
    while not ok_territories_and_wasteland:
        ok_territories = False
        while not ok_territories:
            nb_territories = int(nb_land / AVG_TERRITORY_SIZE)
            logging.info("Try a new set of "+str(nb_territories)+" territories for map "+str(nmap)+" now")
            no_territory = {"x":0, "y":0, "color":(0,0,0), "size":0, "avgheight":0, "wateland":True}
            territorymap = [[no_territory for y in range(bmpsizey+1)] for x in range(bmpsizex+1)]
            territories_centers = []
            for n in range(nb_territories):
                if n%10==9:
                    logging.debug("Creating territory "+str(n+1)+"/"+str(nb_territories))
                ok = False
                nb_tries_2 = 0
                while not ok:
                    x = random.randint(1,bmpsizex)
                    y = random.randint(1,bmpsizey)
                    if heightmap[x][y]<SEA_HEIGHT:
                        continue
                    ok = True
                    color = get_color(False)
                    min_dist_sq = (get_size_edge_mlt(y)**2)*AVG_TERRITORY_SIZE/3
                    for c in territories_centers:
                        if (c["x"]-x)**2 + (c["y"]-y)**2 < min_dist_sq:
                            ok = False
                            break
                    nb_tries_2 += 1
                    if nb_tries_2 > 100:
                        break
                    #if not ok:
                    #    logging.debug("land : not ok")
                if ok:
                    territories_centers += [{"x":x, "y":y, "color":color, "size":0, "avgheight":0, "wasteland":False,
                        "estuary":False, "trade":False, "harbor":False, "region":False, "coast":False, 
                        "distmap":create_distmap(x,y,"byland")}]
            logging.debug("End creating main territories : "+str(len(territories_centers)))
            
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    if heightmap[x][y]>=SEA_HEIGHT:
                        best = None                    
                        best_dist = MAX_DIST
                        for c in territories_centers:
                            dist = c["distmap"][x,y]
                            if dist < best_dist:
                                best_dist = dist
                                best = c
                        if best==None:
                            logging.debug("Add island territory - now "+
                                str(1+len(territories_centers))+" t. centers")
                            color = get_color(False)
                            best = {"x":x, "y":y, "color":color, "size":0, "avgheight":0, "wasteland":False,
                                "estuary":False, "trade":False, "harbor":False, "region":False, "coast":False, 
                                "distmap":create_distmap(x,y,"byland")}
                            territories_centers += [best]
                        territorymap[x][y] = best
                        
            logging.debug("End creating all territories : "+str(len(territories_centers)))
            
            # size
            for x in range(bmpsizex+1):
                for y in range(bmpsizey+1):
                    if territorymap[x][y] in territories_centers:
                        territorymap[x][y]["size"] += 1
                        territorymap[x][y]["avgheight"] += heightmap[x][y]
            for c in territories_centers:            
                c["avgheight"] /= c["size"]
                
            # coasts and estuaries
            for x in range(1,bmpsizex):
                for y in range(1,bmpsizey):
                        if ((heightmap[x-1][y]<SEA_HEIGHT and byseatobordermap[x-1,y]<MAX_DIST) or 
                                (heightmap[x+1][y]<SEA_HEIGHT and byseatobordermap[x+1,y]<MAX_DIST) or
                                (heightmap[x][y-1]<SEA_HEIGHT and byseatobordermap[x,y-1]<MAX_DIST) or 
                                (heightmap[x][y+1]<SEA_HEIGHT and byseatobordermap[x,y+1]<MAX_DIST)):
                            territorymap[x][y]["coast"] = True                        
                        if (heightmap[x-1][y]<SEA_HEIGHT or 
                                    heightmap[x+1][y]<SEA_HEIGHT or
                                    heightmap[x][y-1]<SEA_HEIGHT or 
                                    heightmap[x][y+1]<SEA_HEIGHT):
                            if rivermap[x][y]:
                                territorymap[x][y]["estuary"] = True   
                                             
            ok_territories = True
            nb_tries += 1
            min_size = int(MIN_TERRITORY_SIZE*(3/(2+nb_tries)))
            max_size = int(MAX_TERRITORY_SIZE*((2+nb_tries)/3))
            for c in territories_centers:            
                this_min_size = min_size*get_size_edge_mlt(c["y"])
                this_max_size = max_size*get_size_edge_mlt(c["y"])
                c["neighbours"] = []
                wasteland_lowerer_border = wasteland_lowerer_center = 0
                if north_edge:
                    if south_edge:
                        wasteland_lowerer_border = max(0,40-c["y"],40-(bmpsizey-c["y"]))
                        if sizex*sizey>LIM_RISK_1024:
                            mlt = (sizex*sizey)/LIM_RISK_1024
                            wasteland_lowerer_center = mlt * max(0, 
                                10-abs(c["y"]-NORTH_TO_EQUATOR_DISTANCE)/5)
                    else:
                        wasteland_lowerer_border = max(0,40-c["y"])
                        if sizex*sizey>LIM_RISK_1024:
                            mlt = (sizex*sizey)/LIM_RISK_1024
                            wasteland_lowerer_center = mlt * max(0, 
                                10-abs(c["y"]-NORTH_TO_EQUATOR_DISTANCE)/5)
                if south_edge:
                    wasteland_lowerer_border = max(0,40-(bmpsizey-c["y"]))
                    if sizex*sizey>LIM_RISK_1024:
                        mlt = (sizex*sizey)/LIM_RISK_1024
                        wasteland_lowerer_center = mlt * max(0, 
                            10-abs(c["y"]-bmpsizey-NORTH_TO_EQUATOR_DISTANCE+2048)/5)
                if c["avgheight"]>WASTELAND_HEIGHT-wasteland_lowerer_border-wasteland_lowerer_center:
                    c["wasteland"] = True
                    c["estuary"] = False     
                if c["size"]<this_min_size or (c["size"]>this_max_size and not c["wasteland"]):
                    ok_territories = False
                    logging.info("One territory has a strange size ("+str(c["size"])+" not in ["+
                        str(this_min_size)+"-"+str(this_max_size)+"]), recreating all territories")
                    break   

        # land neighbours
        for x in range(bmpsizex):
            for y in range(bmpsizey):
                if (territorymap[x][y]!=territorymap[x+1][y] and 
                        territorymap[x][y] in territories_centers and
                        territorymap[x+1][y] in territories_centers and
                        territorymap[x+1][y] not in territorymap[x][y]["neighbours"]):
                    territorymap[x][y]["neighbours"] += [territorymap[x+1][y]]
                    territorymap[x+1][y]["neighbours"] += [territorymap[x][y]]                
                if (territorymap[x][y]!=territorymap[x][y+1] and 
                        territorymap[x][y] in territories_centers and
                        territorymap[x][y+1] in territories_centers and
                        territorymap[x][y+1] not in territorymap[x][y]["neighbours"]):
                    territorymap[x][y]["neighbours"] += [territorymap[x][y+1]]
                    territorymap[x][y+1]["neighbours"] += [territorymap[x][y]] 
                    
        wasteland_neighbours_changed = True
        while wasteland_neighbours_changed:
            # Group wastelands
            wasteland_neighbours_changed = False
            for c in territories_centers:
                if (not c["wasteland"]) or c["size"]==0:
                    continue
                for c2 in c["neighbours"]:
                    if c2["wasteland"] and c2["size"]>0 and c!=c2:
                        # logging.debug("Group 2 wastelands : "+str(c["color"])+" and "+str(c2["color"]))
                        for x in range(bmpsizex):
                            for y in range(bmpsizey):            
                                if territorymap[x][y]==c2:
                                    territorymap[x][y]=c    
                        for n in c2["neighbours"]:
                            if n not in c["neighbours"] and n!=c and n["size"]>0:
                                c["neighbours"] += [n]
                        c2["neighbours"] = []
                        c["size"] += c2["size"]
                        c2["size"] = 0
                        c["coast"] |= c2["coast"]
                        wasteland_neighbours_changed = True
            territories_centers = [c for c in territories_centers if c["size"]>0] 
            # isolated territories inside wastelands 
            for c in territories_centers:
                c["neighbours"] = [n for n in c["neighbours"] if n["size"]>0] 
                c["to_coast"] = c["coast"] and not c["wasteland"]
            to_coast_changed = True
            while to_coast_changed:
                to_coast_changed = False
                for c in territories_centers:
                    if (not c["to_coast"]) and (not c["wasteland"]):
                        for c2 in c["neighbours"]:
                            # TODO remove this, this shouldn't be needed
                            if "to_coast" not in c2:
                                logging.warn("to_coast not in c2 : "+str(c2))
                                return False
                            if c2["to_coast"]:
                                c["to_coast"] = True
                                to_coast_changed = True
            for c in territories_centers:
                if (not c["to_coast"]) and (not c["wasteland"]):
                    c["wasteland"] = True
                    c["estuary"] = False
                    wasteland_neighbours_changed = True
                    logging.debug("One more wasteland because being isolated : "+str(c["color"]))

        logging.debug("End joigning wastelands : "+
            str(len([c for c in territories_centers if c["wasteland"]])))       
        
        if len(territories_centers)>MAX_TOTAL_TERRITORIES*0.92: # at least 8% seas+lakes
            logging.info("Already too much total territories ("+str(len(territories_centers))+"), retry")
            mlt = len(territories_centers)/(MAX_TOTAL_TERRITORIES*0.95)+0.05
            MIN_AREA_SIZE = int(MIN_AREA_SIZE*mlt)
            MIN_TERRITORY_SIZE = MIN_AREA_SIZE            
            AVG_TERRITORY_SIZE = int(AVG_TERRITORY_SIZE*mlt)
            MAX_TERRITORY_SIZE = int(MAX_TERRITORY_SIZE*mlt)
        elif len(territories_centers)<MIN_LAND_TERRITORIES_16_16 and sizex==16 and sizey==16:
            logging.info("Too few total territories ("+str(len(territories_centers))+" for 16x16), retry")
            mlt = len(territories_centers)/MIN_LAND_TERRITORIES_16_16-0.05
            MIN_AREA_SIZE = int(MIN_AREA_SIZE*mlt)
            MIN_TERRITORY_SIZE = MIN_AREA_SIZE            
            AVG_TERRITORY_SIZE = int(AVG_TERRITORY_SIZE*mlt)
            MAX_TERRITORY_SIZE = int(MAX_TERRITORY_SIZE*mlt)
        else:
            ok_territories_and_wasteland = True
        if not ok_territories_and_wasteland and nb_tries>100:
            logging.warn("Too much tries with this map ("+str(nb_tries)+"), aborting !")
            os.remove("tiles/data/"+name+"_h.bmp")
            os.remove("tiles/data/"+name+"_r.bmp")
            return False    
    
    # Seas
    ok_seas = False
    nb_tries = 0
    while not ok_seas:    
        nb_seas = int(0.7 * nb_water / AVG_SEA_SIZE) # 0.7 : enough seas (near all islands), but not too much
        for x in range(bmpsizex+1):
            for y in range(bmpsizey+1):        
                if heightmap[x][y]<SEA_HEIGHT:
                    territorymap[x][y] = no_territory
        seas_centers = []
        sea_center_nb_tries = 0
        for n in range(nb_seas):
            ok = False
            while not ok:
                sea_center_nb_tries += 1
                if sea_center_nb_tries+n*10>nb_seas*10: # already too much seas
                    break
                x = random.randint(1,bmpsizex)
                y = random.randint(1,bmpsizey)
                size_edge_mlt = get_size_edge_mlt(y)
                if heightmap[x][y]>=SEA_HEIGHT:
                    continue
                if (map_to_land[x][y]<(AVG_SEA_SIZE**0.5)*0.05*size_edge_mlt 
                        or map_to_land[x][y]>(AVG_SEA_SIZE**0.5)*0.3*size_edge_mlt):
                    # logging.debug("sea center too_far")
                    continue
                ok = True                                                        
                color = get_color(True)
                min_dist_sea = (AVG_SEA_SIZE**0.5)*size_edge_mlt
                for c in seas_centers:
                    if c["distmap"][x,y] < min_dist_sea:
                        ok = False
                        break
                if not ok:
                    # logging.debug("sea : not ok")
                    continue
                sea_center = {"x":x, "y":y, "color":color, "size":0, "avgheight":0, 
                    "lake":False, "neighbours":[]}
            if ok:
                sea_center["distmap"] = create_distmap(x,y,"bysea")
                seas_centers += [sea_center]
        logging.debug("End creating main seas : "+str(len(seas_centers)))

        for x in range(bmpsizex+1):
            for y in range(bmpsizey+1):
                if heightmap[x][y]<SEA_HEIGHT and map_to_land[x][y]<(AVG_SEA_SIZE**0.5):
                    best = None
                    best_dist = MAX_DIST
                    for c in seas_centers:
                        dist = c["distmap"][x,y]
                        if dist < best_dist:
                            best_dist = dist
                            best = c
                    if best==None:
                        logging.debug("Add lake - now "+str(1+len(seas_centers))+" seas")
                        color = get_color(True)
                        best = {"x":x, "y":y, "color":color, "size":0, "avgheight":0, 
                            "lake":True, "distmap":create_distmap(x,y,"bysea"), "neighbours":[]}
                        seas_centers += [best]                        
                    territorymap[x][y] = best
        logging.debug("End creating main seas and lakes : "+str(len(seas_centers)))
        
        # no territory but lake or sea inside some other seas
        bynothingtobordermap = create_distmap(None, None, "bynothingtoborder")
        for x in range(bmpsizex+1):
            for y in range(bmpsizey+1):
                if heightmap[x][y]<SEA_HEIGHT and territorymap[x][y]!=no_territory and byseatobordermap[x,y]>=MAX_DIST:
                    territorymap[x][y]["lake"] = True
                if heightmap[x][y]<SEA_HEIGHT and territorymap[x][y]==no_territory and bynothingtobordermap[x,y]>=MAX_DIST:
                    best = None
                    best_dist = MAX_DIST
                    for c in seas_centers:
                        dist = c["distmap"][x,y]
                        if dist < best_dist:
                            best_dist = dist
                            best = c
                    if best!=None:
                        territorymap[x][y] = best

        # sea but not connected to sea center
        nb_pixels_neutered = 0
        for c in seas_centers:    
            allseadistmap = create_distmap(c["x"], c["y"], "tosamesea")
            for x in range(max(0,c["x"]-3*int(MAX_SEA_SIZE**.5)), 
                    min(bmpsizex+1, c["x"]+3*int(MAX_SEA_SIZE**.5))):
                for y in range(max(0,c["y"]-3*int(MAX_SEA_SIZE**.5)), 
                        min(bmpsizey+1, c["y"]+3*int(MAX_SEA_SIZE**.5))):    
                    if territorymap[x][y] == c and allseadistmap[x,y]>=MAX_DIST:
                        territorymap[x][y] = no_territory
                        nb_pixels_neutered += 1
                        # logging.debug("neuter "+str(x)+","+str(y)+" : "+str(allseadistmap[x,y])+" -> "+str(c["color"]))
        logging.debug("Neutered sea pixels (can't reach sea center) : "+str(nb_pixels_neutered))
        for x in range(1,bmpsizex):
            for y in range(1,bmpsizey):
                if territorymap[x][y] in seas_centers:
                    territorymap[x][y]["size"] += 1
                if (territorymap[x][y]!=territorymap[x+1][y] and 
                        territorymap[x][y] in seas_centers and
                        territorymap[x+1][y] in seas_centers and
                        territorymap[x+1][y] not in territorymap[x][y]["neighbours"]):
                    territorymap[x][y]["neighbours"] += [territorymap[x+1][y]]
                    territorymap[x+1][y]["neighbours"] += [territorymap[x][y]]                
                if (territorymap[x][y]!=territorymap[x][y+1] and 
                        territorymap[x][y] in seas_centers and
                        territorymap[x][y+1] in seas_centers and
                        territorymap[x][y+1] not in territorymap[x][y]["neighbours"]):
                    territorymap[x][y]["neighbours"] += [territorymap[x][y+1]]
                    territorymap[x][y+1]["neighbours"] += [territorymap[x][y]]
        seas_centers = [c for c in seas_centers if c["size"]>0]
        
        ok_seas = True
        nb_tries += 1
        min_size = int(MIN_SEA_SIZE*(7/(6+nb_tries)))
        max_size = int(MAX_SEA_SIZE*((6+nb_tries)/7))
        #group small sea neighbours, even lakes
        grouped_one = True
        while grouped_one:
            grouped_one = False
            for c in seas_centers:            
                this_min_size = min_size * get_size_edge_mlt(c["y"])
                this_max_size = max_size * get_size_edge_mlt(c["y"])
                if c["size"]>0 and c["size"]<this_min_size:
                    best_neighbour = None
                    best_size_neighbour = 1e6
                    for cn in c["neighbours"]:
                        if c!=cn and cn["size"]>0 and cn["size"]<best_size_neighbour:
                            best_neighbour = cn
                            best_size_neighbour = cn["size"]
                    if (best_neighbour!=None) and (best_size_neighbour+c["size"]<this_max_size):
                        logging.debug("Grouping 2 seas : "+str(c["color"])+" in "+str(best_neighbour["color"]))
                        for x in range(bmpsizex+1):
                            for y in range(bmpsizey+1):            
                                if territorymap[x][y]==c:
                                    territorymap[x][y]=best_neighbour
                        best_neighbour["size"] += c["size"]
                        c["size"] = 0
                        best_neighbour["neighbours"] += c["neighbours"]
                        c["neighbours"] = 0
                        grouped_one = True
        seas_centers = [c for c in seas_centers if (c["size"]>0)]
        nb_seas_not_lakes = len([c for c in seas_centers if not c["lake"]])
        if nb_seas_not_lakes>MAX_SEAS_EXCLUDING_LAKES:
            logging.info("Too much seas ("+str(nb_seas_not_lakes)+" > "+str(MAX_SEAS_EXCLUDING_LAKES)+")")
            ok_seas = False
            break
        nb_lakes = len([l for l in seas_centers if l["lake"]])
        if nb_lakes>MAX_LAKES:
            logging.warn("Too much lakes ("+str(nb_lakes)+"), aborting !")
            os.remove("tiles/data/"+name+"_h.bmp")
            os.remove("tiles/data/"+name+"_r.bmp")
            return False
   
        #checks still too small or big seas
        for c in seas_centers:            
            this_min_size = min_size * get_size_edge_mlt(c["y"])
            this_max_size = max_size * get_size_edge_mlt(c["y"])
            if (not c["lake"]) and (c["size"]<this_min_size or c["size"]>this_max_size):
                ok_seas = False                
                logging.info("One sea has a strange size ("+str(c["size"])+" not in ["+
                    str(this_min_size)+"-"+str(this_max_size)+"]), recreating all seas")
                break                
                
        #checks lands without sea neightbour
        for x in range(1,bmpsizex):
            if ok_seas:
                for y in range(1,bmpsizey):
                    if (territorymap[x][y] in territories_centers and
                            ((heightmap[x+1][y]<SEA_HEIGHT and territorymap[x+1][y]==no_territory) or
                            (heightmap[x-1][y]<SEA_HEIGHT and territorymap[x-1][y]==no_territory) or
                            (heightmap[x][y+1]<SEA_HEIGHT and territorymap[x][y+1]==no_territory) or
                            (heightmap[x][y-1]<SEA_HEIGHT and territorymap[x][y-1]==no_territory))):
                        ok_seas = False                
                        logging.info("One land territory without valid sea coast, recreating all seas")
                        break
        
    nb_total_territories = len(territories_centers)+len(seas_centers)
    logging.debug("End creating and removing main seas and lakes : "+str(len(seas_centers))+
                ", total : "+str(nb_total_territories))
    
    if nb_total_territories>MAX_TOTAL_TERRITORIES:
        logging.warn("Too much total territories ("+str(nb_total_territories)+"), aborting !")
        os.remove("tiles/data/"+name+"_h.bmp")
        os.remove("tiles/data/"+name+"_r.bmp")
        return False

    # Assertions
    for x in range(bmpsizex):
        for y in range(bmpsizey):
            if (territorymap[x][y] not in territories_centers and 
                    territorymap[x][y] not in seas_centers and 
                    territorymap[x][y] != no_territory):
                logging.error(str(x)+","+str(y)+" - "+str(territorymap[x][y]["color"])+" is nowhere!")
                return False
    
    # straits, 4 directions
    straits = []
    for x in range(bmpsizex):
        for y in range(bmpsizey-MAX_STRAIT_LEN):
            t1 = territorymap[x][y]
            if not t1 in territories_centers or t1["wasteland"]:
                continue
            t2 = territorymap[x][y+MAX_STRAIT_LEN-1]
            if (not t2 in territories_centers or t2["wasteland"] or t2==t1 or t2 in t1["neighbours"]
                    or t2["distmap"][x][y]<MAX_STRAIT_LEN*50):
                continue
            sea = None
            goodsea = False
            for yy in range(y, y+MAX_STRAIT_LEN):                
                if territorymap[x][yy] in seas_centers:
                    if sea==None:
                        sea = territorymap[x][yy]
                        goodsea = True
                    elif sea!=territorymap[x][yy]:
                        goodsea = False
                        break
                if (territorymap[x][yy] in territories_centers and 
                        not(territorymap[x][yy] in (t1, t2))):
                    goodsea = False
                    break
            if goodsea:
                if ((t1["color"][0]<<16)+(t1["color"][1]<<8)+t1["color"][2] >
                        (t2["color"][0]<<16)+(t2["color"][1]<<8)+t2["color"][2]):
                    ttmp = t1
                    t1 = t2
                    t2 = ttmp
                strait = {"t1":t1, "t2":t2, "sea":sea}
                realnew = True
                for st2 in straits:
                    if (st2["t1"]==t1 and st2["t2"]==t2) or (st2["t1"]==t2 and st2["t2"]==t1):
                        realnew = False
                        break
                    elif (st2["sea"]!=sea) or (st2["t1"]!=t1 and st2["t2"]!=t2 and st2["t1"]!=t2 and st2["t2"]!=t1):
                        realnew = True
                        continue
                    elif (st2["t1"]!=t1 and st2["t2"]==t2 and st2["t1"] in t1["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t1 and st2["t2"]!=t2 and st2["t2"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]!=t2 and st2["t2"]==t1 and st2["t1"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t2 and st2["t2"]!=t1 and st2["t2"] in t1["neighbours"]):
                        realnew = False
                        break
                if realnew:        
                    straits += [strait]
                    logging.debug("Create vertical strait in sea : "+str(sea["color"]))  
    for x in range(bmpsizex-MAX_STRAIT_LEN):
        for y in range(bmpsizey):
            t1 = territorymap[x][y]
            if not t1 in territories_centers or t1["wasteland"]:
                continue
            t2 = territorymap[x+MAX_STRAIT_LEN-1][y]
            if (not t2 in territories_centers or t2["wasteland"] or t2==t1 or t2 in t1["neighbours"]
                    or t2["distmap"][x][y]<MAX_STRAIT_LEN*50):
                continue
            sea = None
            goodsea = False
            for xx in range(x, x+MAX_STRAIT_LEN):                
                if territorymap[xx][y] in seas_centers:
                    if sea==None:
                        sea = territorymap[xx][y]
                        goodsea = True
                    elif sea!=territorymap[xx][y]:
                        goodsea = False
                        break                        
                if (territorymap[xx][y] in territories_centers and 
                        not(territorymap[xx][y] in (t1, t2))):
                    goodsea = False
                    break
            if goodsea:
                if (t1["color"][0]<<16+t1["color"][1]<<8+t1["color"][2] >
                        t2["color"][0]<<16+t2["color"][1]<<8+t2["color"][2]):
                    ttmp = t1
                    t1 = t2
                    t2 = ttmp
                strait = {"t1":t1, "t2":t2, "sea":sea}                
                realnew = True
                for st2 in straits:
                    if (st2["t1"]==t1 and st2["t2"]==t2) or (st2["t1"]==t2 and st2["t2"]==t1):
                        realnew = False
                        break
                    elif (st2["sea"]!=sea) or (st2["t1"]!=t1 and st2["t2"]!=t2 and st2["t1"]!=t2 and st2["t2"]!=t1):
                        realnew = True
                        continue
                    elif (st2["t1"]!=t1 and st2["t2"]==t2 and st2["t1"] in t1["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t1 and st2["t2"]!=t2 and st2["t2"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]!=t2 and st2["t2"]==t1 and st2["t1"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t2 and st2["t2"]!=t1 and st2["t2"] in t1["neighbours"]):
                        realnew = False
                        break
                if realnew:        
                    straits += [strait]
                    logging.debug("Create horizontal strait in sea : "+str(sea["color"]))  

    MAX_STRAIT_LEN_DIAG = int(MAX_STRAIT_LEN * 0.707)
    for x in range(bmpsizex-MAX_STRAIT_LEN_DIAG):
        for y in range(bmpsizey-MAX_STRAIT_LEN_DIAG):
            t1 = territorymap[x][y]
            if not t1 in territories_centers or t1["wasteland"]:
                continue
            t2 = territorymap[x+MAX_STRAIT_LEN_DIAG-1][y+MAX_STRAIT_LEN_DIAG-1]
            if (not t2 in territories_centers or t2["wasteland"] or t2==t1 or t2 in t1["neighbours"]
                    or t2["distmap"][x][y]<MAX_STRAIT_LEN_DIAG*50):
                continue
            sea = None
            goodsea = False
            yy = y 
            for xx in range(x, x+MAX_STRAIT_LEN_DIAG):                
                if territorymap[xx][yy] in seas_centers:
                    if sea==None:
                        sea = territorymap[xx][yy]
                        goodsea = True
                    elif sea!=territorymap[xx][yy]:
                        goodsea = False
                        break
                if (territorymap[xx][yy] in territories_centers and 
                        not(territorymap[xx][yy] in (t1, t2))):
                    goodsea = False
                    break
                yy += 1
            if goodsea:
                if ((t1["color"][0]<<16)+(t1["color"][1]<<8)+t1["color"][2] >
                        (t2["color"][0]<<16)+(t2["color"][1]<<8)+t2["color"][2]):
                    ttmp = t1
                    t1 = t2
                    t2 = ttmp
                strait = {"t1":t1, "t2":t2, "sea":sea}                
                realnew = True
                for st2 in straits:
                    if (st2["t1"]==t1 and st2["t2"]==t2) or (st2["t1"]==t2 and st2["t2"]==t1):
                        realnew = False
                        break
                    elif (st2["sea"]!=sea) or (st2["t1"]!=t1 and st2["t2"]!=t2 and st2["t1"]!=t2 and st2["t2"]!=t1):
                        realnew = True
                        continue
                    elif (st2["t1"]!=t1 and st2["t2"]==t2 and st2["t1"] in t1["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t1 and st2["t2"]!=t2 and st2["t2"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]!=t2 and st2["t2"]==t1 and st2["t1"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t2 and st2["t2"]!=t1 and st2["t2"] in t1["neighbours"]):
                        realnew = False
                        break
                if realnew:        
                    straits += [strait]
                    logging.debug("Create diag\\ strait in sea : "+str(sea["color"]))  
    for x in range(bmpsizex-MAX_STRAIT_LEN_DIAG):
        for y in range(MAX_STRAIT_LEN_DIAG,bmpsizey):
            t1 = territorymap[x][y]
            if not t1 in territories_centers or t1["wasteland"]:
                continue
            t2 = territorymap[x+MAX_STRAIT_LEN_DIAG-1][y-MAX_STRAIT_LEN_DIAG+1]
            if (not t2 in territories_centers or t2["wasteland"] or t2==t1 or t2 in t1["neighbours"]
                    or t2["distmap"][x][y]<MAX_STRAIT_LEN_DIAG*50):
                continue
            sea = None
            goodsea = False
            yy = y
            for xx in range(x, x+MAX_STRAIT_LEN_DIAG):                
                if territorymap[xx][yy] in seas_centers:
                    if sea==None:
                        sea = territorymap[xx][yy]
                        goodsea = True
                    elif sea!=territorymap[xx][yy]:
                        goodsea = False
                        break
                if (territorymap[xx][yy] in territories_centers and 
                        not(territorymap[xx][yy] in (t1, t2))):
                    goodsea = False
                    break
                yy -= 1
            if goodsea:
                if ((t1["color"][0]<<16)+(t1["color"][1]<<8)+t1["color"][2] >
                        (t2["color"][0]<<16)+(t2["color"][1]<<8)+t2["color"][2]):
                    ttmp = t1
                    t1 = t2
                    t2 = ttmp
                strait = {"t1":t1, "t2":t2, "sea":sea}                
                realnew = True
                for st2 in straits:
                    if (st2["t1"]==t1 and st2["t2"]==t2) or (st2["t1"]==t2 and st2["t2"]==t1):
                        realnew = False
                        break
                    elif (st2["sea"]!=sea) or (st2["t1"]!=t1 and st2["t2"]!=t2 and st2["t1"]!=t2 and st2["t2"]!=t1):
                        realnew = True
                        continue
                    elif (st2["t1"]!=t1 and st2["t2"]==t2 and st2["t1"] in t1["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t1 and st2["t2"]!=t2 and st2["t2"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]!=t2 and st2["t2"]==t1 and st2["t1"] in t2["neighbours"]):
                        realnew = False
                        break
                    elif (st2["t1"]==t2 and st2["t2"]!=t1 and st2["t2"] in t1["neighbours"]):
                        realnew = False
                        break
                if realnew:        
                    straits += [strait]
                    logging.debug("Create diag/ strait in sea : "+str(sea["color"]))  

    # BMP : p(rovinces)
    hbmp = Image.new("RGB", (bmpsizex, bmpsizey), "black")
    pixels = hbmp.load()
    for x in range(bmpsizex):
        for y in range(bmpsizey):
            pixels[x, y] = territorymap[x][y]["color"]
    hbmp.save("tiles/data/"+name+"_p.bmp")

    """TODO useless ?
    nb_trade = 0
    nb_estuary = 0
    nb_harbor = 0
    nb_region = 0
    for c in territories_centers:
        if c["trade"]:
            nb_trade += 1
        if c["estuary"]:
            nb_estuary += 1    
        if c["harbor"]:
            nb_harbor += 1    
        if c["region"]:
            nb_region += 1    
    """        
    
    # trade, harbor and regions
    for c in territories_centers:
        if c["wasteland"]:
            continue
        ok = True
        for c2 in territories_centers:
            if (c2["trade"] or c2["harbor"]) and ((c["x"]-c2["x"])**2+(c["y"]-c2["y"])**2)**0.5 < TRADE_CENTER_MIN_DIST:
                ok = False
                break            
            if (c2["estuary"] and ((c["x"]-c2["x"])**2+(c["y"]-c2["y"])**2)**0.5 < TRADE_CENTER_MIN_DIST 
                    and (c2["color"][0]+c["color"][1])%3==0):  #  a bit random
                ok = False
                break                
        if ok:
            if c["coast"] and (c2["color"][0]+c["color"][1])%4==0:  #  a bit random
                c["harbor"] = True
            else:
                c["trade"] = True
    for c in territories_centers:
        if c["wasteland"]:
            continue
        ok = True
        for c2 in territories_centers:
            if c2["region"] and ((c["x"]-c2["x"])**2+(c["y"]-c2["y"])**2)**0.5 < REGION_CENTER_MIN_DIST:
                ok = False
                break            
            if ((c2["estuary"] or c2["trade"] or c2["harbor"]) and 
                ((c["x"]-c2["x"])**2+(c["y"]-c2["y"])**2)**0.5 < TRADE_CENTER_MIN_DIST and 
                (c2["color"][0]+c["color"][1])%5==0):  #  a bit random
                ok = False
                break
        if ok:
            c["region"] = True

    # TXT file
    txt = ""
    nb_seas_not_lake = 0
    for c in seas_centers:
        if c["lake"]:
            txt += "lake_province = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"
        else:            
            txt += "sea_province = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"
            nb_seas_not_lake += 1
    nb_territories_not_wasteland = len([c for c in territories_centers if not c["wasteland"]])
    txt += "num_sea_provinces = "+str(nb_seas_not_lake)+"\n"
    txt += "num_land_provinces = "+str(nb_territories_not_wasteland)+"\n" 
    txt += "size = { "+str(sizex)+" "+str(sizey)+" }\n"
    txt += "continent = yes\n"
    txt += "weight = "+str(int((sizex+sizey)*3))+"\n"
    txt += "empty = { 0 0 0 }\n"
    nb_regions = 0
    for c in territories_centers:
        if c["wasteland"]:
            txt += "wasteland_province = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"
        if c["estuary"]:
            txt += "river_estuary_modifier = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"
        if c["trade"]:
            txt += "level_1_center_of_trade = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"        
        if c["harbor"]:
            txt += "important_natural_harbor = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"
        if c["region"] and sizex*sizey>MIN_SIZE_SET_REGION:
            txt += "region = { "+str(c["color"][0])+" "+str(c["color"][1])+" "+str(c["color"][2])+" }\n"        
            nb_regions += 1
    if sizex*sizey>MIN_SIZE_SET_REGION:
        txt += "regions = "+str(nb_regions)+"\n"
    for strait in straits:
        txt += ("strait = { \n  from = { "+str(strait["t1"]["color"][0])+" "+
            str(strait["t1"]["color"][1])+" "+str(strait["t1"]["color"][2])+" } "+
            " \n  to = { "+str(strait["t2"]["color"][0])+" "+
            str(strait["t2"]["color"][1])+" "+str(strait["t2"]["color"][2])+" } "+
            " \n  through = { "+str(strait["sea"]["color"][0])+" "+
            str(strait["sea"]["color"][1])+" "+str(strait["sea"]["color"][2])+" }\n}\n")  
    if north_edge:
        if south_edge:
            txt += "do_not_rotate = yes\n"    
        else:
            txt += "restrict_to_north_edge = yes\ndo_not_rotate = yes\n"
    elif south_edge:
        txt += "restrict_to_south_edge = yes\ndo_not_rotate = yes\n"    
    txt += "\n# Generated by PyRandomNewWorld, "+starttime_str+"\n"
    with open("tiles/"+name+".txt","w") as fw:
        fw.write(txt)
    
    # massive logging     
    """  
    for c in territories_centers:
        with open(name+"_t_"+str(c["color"][0])+"-"+str(c["color"][1])+"-"+str(c["color"][2])+".csv","w") as f:
            np.savetxt(f, np.swapaxes(c["distmap"],0,1), delimiter=",", fmt="%d")
    """
    """
    for c in seas_centers:
        with open(name+"_s_"+str(c["color"][0])+"-"+str(c["color"][1])+"-"+str(c["color"][2])+".csv","w") as f:
            np.savetxt(f, np.swapaxes(c["distmap"],0,1), delimiter=",", fmt="%d")
    """
    
    logging.info("Generated "+str(nmap)+" : "+str(int(prc_land))+"% land ")
    
    # Finally useless
    """    
    if north_edge and south_edge:
        for bmpletter in ["r","h","p"]:
            with Image.open("tiles/data/"+name+"_"+bmpletter+".bmp") as im:
                imm = ImageOps.mirror(im)
                imm.save("tiles/data/"+name+"r_"+bmpletter+".bmp")
        shutil.copyfile("tiles/"+name+".txt","tiles/"+name+"r.txt")
        logging.info("Generated also a mirror of "+str(nmap))
    """
    
    pp = psutil.Process(os.getpid())
    logging.debug("VMem usage of this processus : "+str(int(pp.memory_info().vms/(2**20)))+"Mb)")
    return True

def generate(n):
    ok = False
    while not ok:
        ok = try_to_generate(n)

def pool_initializer():
    """Needed to catch Ctrl+C"""
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(process)d %(levelname)4s %(message)s")    
if __name__=="__main__":
    if len(sys.argv)>1:
        nb_generations = int(sys.argv[1])
    else:
        nb_generations = 1

    Path("tiles/data").mkdir(parents=True, exist_ok="True")

    if len(sys.argv)>2:
        process_mode = sys.argv[2]
    else:
        process_mode = "newproc"
        
    if len(sys.argv)>3:
        nb_cpu = int(sys.argv[3])
    else:
        nb_cpu = multiprocessing.cpu_count()
    
    logging.info("PyRandomNewWorld : generate new worlds for Europa Universalis 4 (a game by Paradox Interactive)")
    logging.info("Copyright (C) 2024 Askywhale. See README.txt")
    if process_mode == "pool": # faster, more memory
        with multiprocessing.Pool(initializer=pool_initializer) as p:
            try:
                logging.info("Starting generation of {0:d} maps with a pool of {1:d} processes".
                    format(nb_generations, p._processes))
                p.map(generate, range(nb_generations))
            except KeyboardInterrupt:
                logging.warn("KeyboardInterrupt")
                p.terminate()
                p.join()
    elif process_mode == "newproc": # slower, min memory
        logging.info("Starting generation of {0:d} maps with serialized processes".
            format(nb_generations))
        for n in range(nb_generations):
            p = multiprocessing.Process(target=generate, args=(n,)) 
            p.start()
            p.join()            
    elif process_mode == "multiproc": # faster, low memory
        logging.info("Starting generation of {0:d} maps with parallel serialized processes".
            format(nb_generations))        
        n_generation = 0
        n_generation_lock = threading.Lock()
        def thread_run():
            global n_generation, n_generation_lock
            ended = False
            while not ended:                
                with n_generation_lock:                
                    ended = n_generation>=nb_generations
                    this_n = n_generation
                    n_generation += 1
                if not ended:
                    p = multiprocessing.Process(target=generate, args=(this_n,)) 
                    p.start()
                    p.join()
        threads = []
        for n in range(nb_cpu):
            thread = threading.Thread(target=thread_run)
            threads += [thread]
            thread.start()            
        for thread in threads:
            thread.join()            
    elif process_mode == "oneproc": # slower, basic
        logging.info("Starting generation of {0:d} maps in main processes".format(nb_generations))
        for n in range(nb_generations):
            generate(n)
    else:
        logging.error("Unknow processus mode : "+process_mode)

"""
* Notes
Number of provinces (sources : official Wiki 1.36 + CIA WF:W) :
Africa : 462 - 30Mkm²
Asia : 1636 - 44.6Mkm²
Europe : 2485 - 9.9Mkm²
North America : 560 - 24.5Mkm²
Oceania : 95 - 7.7Mkm²
South America : 254 - 17.8Mkm²
-> Goal of this generator : 500 - 1500 (814), on 288 (18*16) tiles of 128*128 pixels
"""