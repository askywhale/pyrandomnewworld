PyRandomNewWorld : generate new worlds for Europa Universalis 4 (a game by Paradox Interactive)
Copyright (C) 2024 Askywhale

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

In Europa Universalis 4, map/random contains a config file, tweaks.lua, that is almost useless.
Instead, the random maps are created from a few tiles (120), dispatched on a 18x16 grid, usually creating a island new world. Each tile is a 4 files set : 1 text file and 3 bitmaps.
This script is a rework of internal Paradox generator, in order to produce bigger and nicer lands.
	
How to use : 
- download and install python 3.8+
- install NumPy Python library : pip install numpy
- install Pillow Python library : pip install pillow
- install PsUtil Python library : pip install psutil
- run me using this command inside this folder : python pyrandomnewworld.py 27 (choose a number, like 27, the quantity of generated tiles) 
- wait (usually a few hours to a few days)
- replace euiv/map/random/tiles/ with the generated ones (keep the old "water*" files, or even all the old files)
- optional : change tweaks.lua > RandomWorld.terrain to get more plains (or the opposite), for example :
- - tropical_heat = 245,
- - subtropic_heat = 225,
- - temperate_heat = 200, 
- generate new world using those tiles :
- - in the begininng of any game ("random new world")
- - (testing) using in the EuIV console : 
- - - ti
- - - map_random seed=-1

